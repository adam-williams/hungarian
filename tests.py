__author__ = 'adam'

import unittest
import hungarian
from hungarian import Matcher

class IntegrationTests(unittest.TestCase):
    def test_invalid_matrix(self):
        with self.assertRaises(AssertionError):
            cover = hungarian.LineCover([[]])
            cover.get_least_lines()

    def test_one_line_matrix(self):
        cover = hungarian.LineCover([
            [50, 00, 50],
            [20, 52, 30],
            [71, 81, 19]
        ])
        self.assertEqual(cover.get_least_lines(), 1)

    def test_two_line_matrix(self):
        cover = hungarian.LineCover([
            [50, 0, 50],
            [20, 0, 30],
            [00, 0, 00]
        ])
        self.assertEqual(cover.get_least_lines(), 2)

    def test_three_line_matrix(self):
        cover = hungarian.LineCover([[30, 0, 30], [0, 0, 10], [0, 20, 0]])
        self.assertEqual(cover.get_least_lines(), 3)

    def test_another_three_line_matrix(self):
        cover = hungarian.LineCover([[9, 0, 3, 0],
                                     [0, 10, 4, 1],
                                     [4, 5, 0, 4],
                                     [0, 2, 4, 6]]
        )
        self.assertEqual(cover.get_least_lines(), 3)

    def test_another_four_line_matrix(self):
        matrix = [
            [10, 0, 3, 0],
            [0, 9, 3, 0],
            [5, 5, 0, 4],
            [0, 1, 3, 5]
        ]
        cover = hungarian.LineCover(matrix)
        self.assertEqual(cover.get_least_lines(), 4)

    def test_four_line_matrix(self):
        cover = hungarian.LineCover([
            [7, 0, 3, 0],
            [0, 9, 3, 0],
            [5, 5, 0, 4],
            [0, 1, 3, 5]
        ])
        self.assertEqual(cover.get_least_lines(), 4)

    def test_five_line_matrix(self):
        cover = hungarian.LineCover([
            [0, 1, 1, 1, 1],
            [1, 0, 1, 1, 1],
            [1, 1, 0, 1, 1],
            [1, 1, 1, 0, 1],
            [1, 1, 1, 1, 0]
        ])
        self.assertEqual(cover.get_least_lines(), 5)

    def test_reduce_rows(self):
        matrix = [
            [14, 5, 8, 7],
            [2, 12, 6, 5],
            [7, 8, 3, 9],
            [2, 4, 6, 10]
        ]
        hun = hungarian.Hungarian(matrix)
        hun.reduce_rows()
        self.assertEqual(hun.get_matrix(), [[9, 0, 3, 2], [0, 10, 4, 3], [4, 5, 0, 6], [0, 2, 4, 8]])

    def test_reduce_cols(self):
        matrix = [
            [9, 0, 3, 2],
            [0, 10, 4, 3],
            [4, 5, 0, 6],
            [0, 2, 4, 8]
        ]
        hun = hungarian.Hungarian(matrix)
        hun.reduce_cols()
        self.assertEqual(hun.get_matrix(), [[9, 0, 3, 0], [0, 10, 4, 1], [4, 5, 0, 4], [0, 2, 4, 6]])

    def test_full_solve(self):
        matrix = [
            [10, 0, 3, 0],
            [0, 9, 3, 0],
            [5, 5, 0, 4],
            [0, 1, 3, 5]
        ]
        hun = hungarian.Hungarian(matrix)
        self.assertEqual(hun.do_solve(), [1, 3, 2, 0])

    def test_another_full_solve(self):
        matrix = [
            [129, 127, 122, 134, 135],
            [127, 125, 123, 131, 132],
            [142, 131, 121, 140, 139],
            [127, 127, 122, 131, 136],
            [141, 134, 129, 144, 143]
        ]
        hun = hungarian.Hungarian(matrix)
        self.assertEqual(hun.do_solve(), [0, 4, 2, 3, 1])
        self.assertEqual(hun.get_cost(), 647)

    def test_matcher(self):
        matrix = [
            [10, 0, 3, 0],
            [0, 9, 3, 0],
            [5, 5, 0, 4],
            [0, 1, 3, 5]
        ]
        self.assertEqual(Matcher().find_zero_locations(matrix), [[1, 3], [0, 3], [2], [0]])

    def test_another_three_liner(self):
        cover = hungarian.LineCover([
            [0, 4, 0, 4],
            [1, 2, 8, 0],
            [1, 0, 2, 0],
            [1, 5, 10, 0]
        ])
        self.assertEqual(cover.get_least_lines(), 3)

    def test_exam_question(self):
        cover = hungarian.LineCover([
            [0, 4, 0, 5],
            [0, 1, 7, 0],
            [1, 0, 2, 1],
            [0, 4, 9, 0]
        ])
        self.assertEqual(cover.get_least_lines(), 4)



if __name__ == '__main__':
    unittest.main()
