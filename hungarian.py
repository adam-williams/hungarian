import copy
import itertools
import json

__author__ = 'adam'


class Hungarian:
    # Current matrix
    matrix = []
    cost_matrix = []
    allocation = []

    def __init__(self, matrix):
        assert_square(matrix)
        self.matrix = matrix
        self.cost_matrix = copy.deepcopy(matrix)

    def reduce_rows(self):
        for i in range(0, len(self.matrix)):
            min_element = None
            for j in range(0, len(self.matrix[i])):
                if min_element is None or self.matrix[i][j] < min_element:
                    min_element = self.matrix[i][j]
            for j in range(0, len(self.matrix[i])):
                self.matrix[i][j] = self.matrix[i][j] - min_element

    def reduce_cols(self):
        for i in range(0, len(self.matrix)):
            min_element = None
            for j in range(0, len(self.matrix)):
                if min_element is None or self.matrix[j][i] < min_element:
                    min_element = self.matrix[j][i]
            for j in range(0, len(self.matrix)):
                self.matrix[j][i] = self.matrix[j][i] - min_element

    def get_cost(self):
        total_cost = 0
        for i, item in enumerate(self.allocation):
            total_cost += self.cost_matrix[i][item]
        return total_cost

    def do_solve(self, print=False):
        self.reduce_rows()
        if print:
            print_json_step_status(self.matrix, "Reduce rows")

        self.reduce_cols()
        if print:
            print_json_step_status(self.matrix, "Reduce columns")

        is_solved = False
        while not is_solved:
            cover = LineCover(self.matrix)
            number_of_lines = cover.get_least_lines()
            if print:
                print_json_step_status(self.matrix, str(number_of_lines) + " is the minimum number of lines required", {"lines": cover.final})
            if number_of_lines >= len(self.matrix):
                is_solved = True
            else:
                e = self.transform(cover.get_final())
                print_json_step_status(self.matrix, "Using e = " + str(e) + ", transform the matrix")
        self.allocation = Matcher.get_winning_combination(
            Matcher.get_combinations(
                Matcher.find_zero_locations(self.matrix)
            ), self.matrix
        )
        return self.allocation

    def get_matrix(self):
        return self.matrix

    def transform(self, lines: list):
        covered_rows = []
        covered_cols = []
        uncovered_elements = []
        for item in lines:
            if item['type'] == 'row':
                covered_rows.append(item['index'])
            else:
                covered_cols.append(item['index'])

        for i, row in enumerate(self.matrix):
            if i in covered_rows:
                continue
            for j, cell in enumerate(row):
                if j in covered_cols:
                    continue
                uncovered_elements.append(cell)

        min_element = min(uncovered_elements)

        i = 0
        for row in self.matrix:
            is_row_covered = False
            if i in covered_rows:
                is_row_covered = True
            j = 0
            for cell in row:
                is_col_covered = False
                if j in covered_cols:
                    is_col_covered = True
                if is_col_covered and is_row_covered:
                    self.matrix[i][j] = cell + min_element
                elif not is_row_covered and not is_col_covered:
                    self.matrix[i][j] = cell - min_element
                j += 1
            i += 1
        return min_element


class LineCover:
    matrix = []
    n = 0
    considered = {"col": {}, "row": {}, "all": []}
    total_zeroes = 0
    final = None

    def __init__(self, matrix):
        assert_square(matrix)
        self.matrix = matrix
        self.considered = {"col": {}, "row": {}, "all": []}
        self.total_zeroes = 0
        self.final = None
        self.n = len(matrix)  # We know it's a square matrix at this point

    def get_value_at_xy(self, x, y):
        return self.matrix[y][x]

    def traverse_axis(self, x):
        element_name = "col" if not x else "row"

        for i in range(0, self.n):
            self.considered[element_name][i] = {"locations": []}
            zero_count = 0

            for el in range(0, self.n):
                if x:
                    val = self.get_value_at_xy(el, i)
                else:
                    val = self.get_value_at_xy(i, el)

                if x and val == 0:
                    self.total_zeroes += 1

                if val == 0:
                    zero_count += 1
                    self.considered[element_name][i]['locations'].append(el)
            self.considered[element_name][i]['total'] = zero_count
            self.considered["all"].append(
                {"locs": self.considered[element_name][i]['locations'], "index": i, "type": element_name,
                 "total": zero_count})

    def create_remaining_zeroes(self):
        remaining_zeroes = [[None for k in range(0, self.n)] for k in range(0, self.n)]
        for x in self.considered['row']:
            for y in self.considered['row'][x]['locations']:
                remaining_zeroes[x][y] = 1
        return remaining_zeroes

    @staticmethod  # j index
    def would_be_beneficial(remaining_zeroes, x, y, type_name):
        if type_name == "row":
            return remaining_zeroes[y][x] is not None

        return remaining_zeroes[x][y] is not None

    def reorder(self):
        ordered_all_indexes = sorted(range(len(self.considered['all'])),
                                     key=lambda k: self.considered['all'][k]['total'],
                                     reverse=True)
        ordered_all = [self.merge_two_dicts(self.considered['all'][k], {"real-index": k}) for k in ordered_all_indexes]
        return ordered_all

    @staticmethod
    def get_next_item(ordered_all):
        if len(ordered_all) == 0:
            return None
        return ordered_all[0]

    @staticmethod
    def merge_two_dicts(x, y):
        """Given two dicts, merge them into a new dict as a shallow copy."""
        z = x.copy()
        z.update(y)
        return z

    def process_decrement(self, remaining_zeroes):
        for k, item in enumerate(self.considered['all']):
            new_total = 0
            if item['type'] == "row":
                # the index is y, the location is x
                for loc in item['locs']:
                    if remaining_zeroes[item['index']][loc] is not None:
                        new_total += 1
            else:
                # the index is x, the location is y
                for loc in item['locs']:
                    if remaining_zeroes[loc][item['index']] is not None:
                        new_total += 1
            item['total'] = new_total

    def pick_lines(self):
        remaining_zeroes = self.create_remaining_zeroes()
        final = []

        while self.total_zeroes > 0:
            ordered_all = self.reorder()

            item = self.get_next_item(ordered_all)

            if item is None:
                break

            add_beneficial = False
            index = item['index']
            for j in range(0, self.n):
                if self.would_be_beneficial(remaining_zeroes, j, index, item['type']):

                    add_beneficial = True
                    self.total_zeroes -= 1
                    if item['type'] == "row":
                        remaining_zeroes[index][j] = None

                        self.process_decrement(remaining_zeroes)
                    else:
                        remaining_zeroes[j][index] = None

                        self.process_decrement(remaining_zeroes)

            if add_beneficial:
                final.append(item)

            if self.total_zeroes == 0:
                break

            del self.considered['all'][item['real-index']]

        return final

    def get_least_lines(self):
        self.traverse_axis(True)
        self.traverse_axis(False)

        self.final = self.pick_lines()
        return len(self.final)

    def get_final(self) -> list:
        return self.final

class Matcher:

    @staticmethod
    def find_zero_locations(final_matrix: list) -> list:
        return [[i for i, c in enumerate(row) if c == 0] for row in final_matrix]

    @staticmethod
    def get_combinations(zeroes):
        return list(map(list, itertools.product(*zeroes)))

    @staticmethod
    def get_winning_combination(combinations, final_matrix):
        for x in combinations:
            if len(set(x)) == len(final_matrix):
                return x

def assert_square(arr):
    counts = []
    [counts.append(len(x)) for x in arr]
    for x in counts:
        if x != len(arr):
            raise AssertionError("Not a square matrix!")

def print_json_error(error: str):
    print(json.dumps({"error": error}))
    exit(-1)

def print_json_step_status(matrix: list, description: str, extra: dict=None):
    if extra is None:
        extra = {}
    extra['description'] = description
    extra['matrix'] = matrix
    extra['type'] = "step"
    print(json.dumps(extra))

if __name__ == "__main__":
    item = input()
    try:
        item = json.loads(item)
        assert_square(item)
        for x in item:
            for y in x:
                if not str(y).isdigit():
                    raise TypeError
    except ValueError:
        print_json_error("Invalid problem input (invalid JSON).")
    except AssertionError:
        print_json_error("Invalid problem input (not a square matrix).")
    except TypeError:
        print_json_error("Invalid problem input (non-numeric).")

    h = Hungarian(item)
    allocation = h.do_solve(True)
    print(json.dumps({"type": "final_allocation", "matrix": allocation, "cost": h.get_cost()}))
